

import urllib.parse as urlparse


def url_get_params(url) -> dict:
    params_list = urlparse.parse_qs(url, keep_blank_values=True, encoding="utf8")

    res = {k: params_list[k][0] for k in params_list}
    if res == "":
        return {}

    return res
