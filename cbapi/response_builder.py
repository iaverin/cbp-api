"""
build responses in cbapi format for different types of messages

types:
"system":
    {
        "type":"system",
        "state":"bot",
        .....
    }

"text":
    {"type": "text",
    "text": "Hello world!"}

"typing":
    {"type": "typing",
    "value": true}

"buttons":
    {
        "type":"buttons",
        "title": "Choose next action",
        "buttons": [{
                "title": "Discover price",
                "payload": "PRICE"
            },
            {
                "title": "Connect to operator",
                "payload": "CONNECT"
            }
        ]
    }

"""
import cbapi.errors
import json

def cbapi_system(resp_dict):
    response = dict()
    response["type"] = "system"
    response.update(resp_dict)
    return response


def cbapi_user_message(text: str):
    response = dict()
    response["type"] = "user_message"
    response["text"] = text
    return response


def cbapi_expert_only_message(text: str):
    response = dict()
    response["type"] = "expert_only"
    response["text"] = text
    return response


def cbapi_text(text: str) -> dict:
    """
    creates single responce for text
    """

    if not text:
        raise cbapi.errors.CreateElementEmptyParameter("Text: Empty value")

    return {
        "type": "text",
        "text": text
    }


def cbapi_typing(value=True):
    """
    creates single typings response
    """

    return {
        "type": "typing",
        "value": value
    }


def cbapi_buttons(title: str, buttons: list) -> dict:
    """
    creates single response with buttons. Raises CreateResponseError if any button has empty title or payload

    Args:
        title (str): string with buttons section caption. Could be empty.
        buttons (list): list of dicts with buttons description {"title": "str","payload": "str" ,
                                                                "userReply":"Optional displayed User reply"}

    Returns:
        dict: cbapi response for buttons
    """
    # buttons_to_add = list()
    for button in buttons:
        if not ("title" in button and "payload" in button and button["title"] and button["payload"]):
            raise cbapi.errors.CreateElementEmptyParameter("Button: Empty value")

    return {
        "title": title,
        "buttons": list(buttons),
        "type": "buttons"
    }


def response_from_elements(user_id: str, elements: list, flow_state: dict = None):
    """
    returns response dict for sending to chat client

    Args:
        user_id: string id of user
        elements: list of content elements
        flow_state: dict with user's bot state
    """

    response = {
        "userId": user_id,
        "responses": elements
    }

    if flow_state :
        response.update({"flowState": flow_state})

    return response

