import cbapi.botpress as botpress
import copy
import requests

# global config
global lock

global_auth_data = None

def init(configuration, thread_lock = None):
    global BASE_SERVER
    global BOT_ID
    global config
    global lock

    BASE_SERVER = configuration.BASE_SERVER
    BOT_ID = configuration.BOT_ID
    config = configuration
    lock = thread_lock


def do_auth(authorization):
    resp = botpress.do_auth(config.BASE_SERVER, authorization)
    return resp


def auth_data_equals(one, two):
    return one["payload"]["token"] == two["payload"]["token"]


def request_message(user_id: str, payload: dict, auth_data=None):
    """
    returns the list of elements from botpress request with text
    """
    global global_auth_data
    auth_retry_attempts = 0

    local_auth_data = copy.deepcopy(auth_data)

    while local_auth_data and auth_retry_attempts < 5:

        auth_retry_attempts += 1

        with lock:
            if not auth_data_equals(local_auth_data, global_auth_data):
                local_auth_data = copy.deepcopy(global_auth_data)

        try:
            external_response = botpress.request_for_message(BASE_SERVER, BOT_ID, user_id, payload,
                                                             local_auth_data["payload"]["token"])

            return external_response

        except requests.exceptions.HTTPError as http_error:
            print("Auth error" + str(http_error))
            print("Will retry")
            with lock:
                if auth_data_equals(local_auth_data, global_auth_data):
                    try:
                        print("Trying to authorize...")
                        global_auth_data = do_auth(config.authorization)
                        print("... OK")

                    except Exception as e:
                        print("... Could not authorize:" + str(e))
                        global_auth_data = None
                        local_auth_data = None
                else:
                    print("Seems that credentials have been updated...")

        except Exception as e:
            print("Error" + str(e))
            local_auth_data = None
            return None

    # fallback to unauthorized request
    external_response = botpress.request_for_message(BASE_SERVER, BOT_ID, user_id, payload, auth_data=None)

    return external_response
