class CreateElementEmptyParameter(Exception):
    """ raises when creating cbapi response element and any parameter is empty """
    pass


class NoResponseElementsInBody(Exception):
    """
    raises when proceeding/converting response body from external chat bot platform
    and there is no content elements in response body
    """
    pass
