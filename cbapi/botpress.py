"""
converter from botpress responses to cbapi format. Extracts chat elements from response body.
botpress sample response:
{
        "responses": [
            {
                "type": "typing",
                "value": true
            },
            {
                "type": "text",
                "markdown": true,
                "text": "text"
            },
            {
                "type": "text",
                "text": '{"type":"system", "state":"expert"}'],

                {
                "type": "text",
                "text": '{"type":"user_message", "text":"this message is from user"}'],

            {
                "type": "custom",
                "module": "channel-web",
                "component": "QuickReplies",
                "quick_replies": [
                    {
                        "title": "Discover Price",
                        "payload": "PRICE"
                    },
                    {
                        "title": "Connect to operator",
                        "payload": "CONNECT"
                    },
                    {
                       "title": '{"title":"Optional userReply ", "userReply":"reply 1"}',
                       "payload": "REPLY1"
                    }
                ],
                "wrapped": {
                    "type": "text",
                    "text": "Choose your action",
                    "typing": true
                }
            }
        ]
}
"""

import cbapi
from cbapi import errors
import json
import requests

AUTHORIZATION_ROUTE = "/api/v1/auth/login/basic/default"
MESSAGE_ROUTE = "/api/v1/bots/{bot_id}/converse/{user_id}"
AUTHORIZED_MESSAGE_ROUTE = "/api/v1/bots/{bot_id}/converse/{user_id}/secured?include=state"

# auth_data = None
is_auth = False
token = None


def post_request(base_url, route, payload):
    """
    Requests botpress and throws on errors.

    Args:
        base_url: [http://localhost:3000]
        route:  [/api/v1/converse/<user_id>]
        payload:  dict

    Returns:
         dict with botpress response
    """

    r = requests.post(base_url + route, data=payload)

    if r.status_code == 200:
        resp = r.json()
        return resp
    else:
        r.raise_for_status()


def do_auth(server_url: str, auth_data: dict):
    """
    Returns: dict with full response fron botpress
    """
    payload = {
        "email": auth_data["email"],
        "password": auth_data["password"]
    }

    resp = post_request(server_url, AUTHORIZATION_ROUTE, payload)
    global is_auth, token

    token = resp["payload"]["token"]
    is_auth = True
    return resp


def post_authorized_request(base_url, route, payload, auth_token):
    """
    this should raise the exception only if authorization is bad
    """
    if auth_token is None:
        raise Exception("Token not set")

    r = requests.post(base_url + route, data=payload,
                      headers={"Authorization": "Bearer " + auth_token})

    if r.status_code == 400:
        r.raise_for_status()

    return r.json()


def request_for_message(base_url, bot_id, user_id, payload, auth_data=None):
    if auth_data:
        message_route = AUTHORIZED_MESSAGE_ROUTE.format(bot_id=bot_id, user_id=user_id)
        resp = post_authorized_request(base_url, message_route, payload, auth_data)

    else:
        message_route = MESSAGE_ROUTE.format(bot_id=bot_id, user_id=user_id)
        resp = post_request(base_url, message_route, payload)

    return resp


def extract_elements(response_body: dict) -> list:
    """
    proceed botpress response and extract elements in cbapi format

    Args:
        response_body (dict): dict with full response from Botpress

    Returns:
        list: list of chat elements in cbapi format
    """
    cbapi_response_elements = list()

    if "responses" not in response_body:
        raise errors.NoResponseElementsInBody("Botpress Converter: No <responses> field in resp body")

    for response in response_body["responses"]:

        if response["type"] == "text":

            # if text response is json and contains "system" key, so we treat it like a system type message
            # otherwise parse it as a simple text element 
            try:
                json_from_text = json.loads(response["text"])

                if json_from_text["type"] == "system":
                    cbapi_response_elements.append(cbapi.cbapi_system(json_from_text))

                if json_from_text["type"] == "user_message":
                    cbapi_response_elements.append(cbapi.cbapi_user_message(json_from_text["text"]))

                if json_from_text["type"] == "expert_only":
                    cbapi_response_elements.append(cbapi.cbapi_expert_only_message(json_from_text["text"]))

            except:
                cbapi_response_elements.append(cbapi.cbapi_text(response["text"]))

        if response["type"] == "typing":
            cbapi_response_elements.append(cbapi.cbapi_typing(response["value"]))

        if response["type"] == "custom" and response["component"] == "QuickReplies":
            if "text" not in response["wrapped"]:
                response["wrapped"]["text"] = ""
            buttons = list()
            for reply in response.get("quick_replies", []):
                button = dict(reply)

                try:
                    json_from_text = json.loads(button["title"])
                    button["title"] = json_from_text["title"]
                    button["userReply"] = json_from_text["userReply"]
                except Exception as e:
                    pass

                buttons.append(button)

            cbapi_response_elements.append(

                cbapi.cbapi_buttons(title=response["wrapped"]["text"], buttons=buttons)
            )

    return cbapi_response_elements


def _extract_flow_name(node: str):
    """
    skills/choice-75da02.flow.json
    """
    if not node:
        return ""

    begin_cut = node.find("/")

    end_cut = node.rfind(".flow.json")
    if end_cut > begin_cut:
        return node[begin_cut + 1:end_cut]

    return node


def extract_flow_state(response_body: dict) -> dict:
    """
    extracts and proceeds state information from botpress response

    Args:
        response_body:

    Returns:
        dict with state
    """

    flow_state = dict()
    context = response_body.get("state", dict()).get("context", None)
    if context is not None:
        flow_state = {
            "currentFlow": context.get("currentFlow"),
            "currentNode": context.get("currentNode"),
            "supplemental": {
                "previousFlow": context.get("previousFlow"),
                "previousNode": context.get("previousNode")
            }
        }

        sub_flow = _extract_flow_name(flow_state.get("currentFlow"))
        if sub_flow + ".flow.json" != flow_state["currentFlow"] and flow_state["supplemental"]["previousFlow"]:
            flow_state["currentFlow"] = flow_state["supplemental"]["previousFlow"]
            flow_state["currentNode"] = flow_state["supplemental"]["previousNode"]
            flow_state["supplemental"].update({
                "original": {
                    "currentFlow": context.get("currentFlow"),
                    "currentNode": context.get("currentNode"),
                    "supplemental": {
                        "previousFlow": context.get("previousFlow"),
                        "previousNode": context.get("previousNode")
                    }
                }
            })

        flow_state["currentFlow"] = _extract_flow_name(flow_state["currentFlow"])
        flow_state["supplemental"]["previousFlow"] = _extract_flow_name(flow_state["supplemental"]["previousFlow"])

    return flow_state


def payload_for_data_in_text(data: dict, request_type="text"):
    """
    create payload for data which should be inserted into text text message as JSON
    Args
        data (dict): data
        request_type (str): botpress'es converse API request type ("text")

    Returns
        (str) for putting into http request body
    """
    resp = dict()
    resp["type"] = "text"
    resp["text"] = json.dumps(data, ensure_ascii=False)

    payload_str = '{"type":"text", "text":"' + resp["text"] + '"}'

    # return json.dumps(resp, ensure_ascii=False)
    return resp


def init_request(profiler_response, profile):
    """
    Creates payload for initial request from profile and profiler response
    Args
        profiler_response (dict): data from the profiler
        profile (dict): profiler provided by client

    Returns (str): string payload for http request body
    """

    if profiler_response is None:
        return None

    data = dict()

    if "flow" in profiler_response:
        # call botpress and jump to the flow
        data["command"] = "jumpToFlow"
        data["flow"] = profiler_response["flow"]

        if "node" in profiler_response:
            data["node"] = profiler_response["node"]

    data["vars"] = dict()
    data["vars"]["profile"] = profile

    return payload_for_data_in_text(data)
