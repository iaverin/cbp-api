# cb-api
ChatBot API service. 
Provides integration layer between chat-bot platform (botpress) and target backend.   

Requirements: python 3.6 with pip 

# Installation: 
```
python -m pip install -r requirements.txt
```

Tests: 
```
python -m unittest -v -b
```


Configuration: 
1. Copy _config_default.py into config.py and

2. Set botpress host and botId:

   ```python
   BASE_SERVER = "http://localhost:3000"
   BOT_ID = "flow-test1"
   ```

3. Set the login and password for botpress. Comment lines if no authorisatoin 
required. Without authorisation no additional data would be provided (current flow and node). 
    ```python
   authorization = {
            "email": "email@email.com",
            "password": "pass"
       }     
    ``` 


# Usage (windows): 
~~~
export FLASK_APP=cbserver.py
flask run
~~~

or launch run.bat . 


For development/debugging:
```
export FLASK_ENV=development
```


(c) Ivan Averin
 