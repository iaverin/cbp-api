# -*- coding: utf-8 -*-
import unittest
# from .context import cbapi
import cbapi
from cbapi import botpress
import json
import requests

class ExtractElementsFromBotpressResponseTestSuite(unittest.TestCase):

    def test_elements_from_botpress_responses(self):
        botpress_response_body = {
            "responses": [
                {"type": "text",
                 "text": "Hello World"},
                {"type": "text",
                 "text": "bye"},
                {"type": "typing",
                 "value": True},
                {"type": "custom",
                 "module": "channel-web",
                 "component": "QuickReplies",
                 "quick_replies": [
                     {
                         "title": "Discover price",
                         "payload": "PRICE"
                     },
                     {
                         "title": "Connect to operator",
                         "payload": "CONNECT"
                     },

                     {
                         "title": '{"title":"Title of button 1", "userReply":"reply 1"}',
                         "payload": "REPLY1"
                     },
                     {

                             "title": '{"title":"Title of button 2", "userReply":"reply 2"}',
                             "payload": "REPLY2"
                     },
                     {


                             "title": '{"title":"Wrong user reply", "user2Reply":"reply 2"}',
                             "payload": "REPLY3"
                     }
                 ],
                 "wrapped": {
                     "type": "text",
                     "text": "Choose your action",
                     "typing": True
                 }
                 }
            ]
        }

        valid_cbapi_response = [{"type": "text", "text": "Hello World"}, {"type": "text", "text": "bye"},
                                {"type": "typing", "value": True},
                                {
                                    "type": "buttons",
                                    "title": "Choose your action",
                                    "buttons": [{
                                        "title": "Discover price",
                                        "payload": "PRICE"
                                    },
                                        {
                                            "title": "Connect to operator",
                                            "payload": "CONNECT"},
                                        {
                                            "title": "Title of button 1",
                                            "userReply": "reply 1",
                                            "payload": "REPLY1"
                                        },

                                        {
                                            "title": "Title of button 2",
                                            "userReply": "reply 2",
                                            "payload": "REPLY2"
                                        },
                                        {
                                            "title": "Wrong user reply",
                                            "payload": "REPLY3"
                                        },

                                    ]}
                                ]

        elements_from_response = botpress.extract_elements(botpress_response_body)
        self.assertListEqual(valid_cbapi_response, elements_from_response)

    def test_fail_elements_from_botpress_responses(self):
        botpress_response_body = {
            "fail": [{"type": "text",
                      "text": "Hello World"}]}

        with self.assertRaises(cbapi.errors.NoResponseElementsInBody) as cm:
            elements_from_response = botpress.extract_elements(botpress_response_body)

        e = cm.exception
        self.assertEqual(type(e), cbapi.errors.NoResponseElementsInBody)

        # wrong content inside
        botpress_response_body = {
            "responses": [{"type": "text",
                           "text": ""}]}

        with self.assertRaises(cbapi.errors.CreateElementEmptyParameter) as cm:
            elements_from_response = botpress.extract_elements(botpress_response_body)

        e = cm.exception
        self.assertEqual(type(e), cbapi.errors.CreateElementEmptyParameter)

    def test_system_elements(self):
        botpress_response_body = {
            "responses": [
                {"type": "text",
                 "text": '{"type":"system", "state":"expert"}'}
            ]}

        elements_from_response = botpress.extract_elements(botpress_response_body)

        valid_cbapi_response = [
            {"type": "system",
             "state": "expert"}
        ]
        self.assertListEqual(valid_cbapi_response, elements_from_response)

    def test_botpress_payload(self):
        data = {"command": "jumpToFlow",
                 "flow": "main" }

        payload = botpress.payload_for_data_in_text(data, "text")
        # payload_dict = json.loads(payload)
        payload_text = json.loads(payload["text"])

        self.assertDictEqual(payload, {
            "type": "text",
            "text": '{"command": "jumpToFlow", "flow": "main"}'
        })

    def test_init_request(self):
        profile = dict()
        profile["segment"] = "new"
        profile["search"] = "карбюратор"

        profiler_response = dict()
        profiler_response["state"] = "bot"
        profiler_response["flow"] = "main"
        profiler_response["node"] = "set_initial_state"

        request_payload = botpress.init_request(profiler_response, profile)

        self.assertDictEqual(request_payload, {
            "type": "text",
            "text": '{"command": "jumpToFlow", "flow": "main",'
                   ' "node": "set_initial_state", "vars": {"profile": {"segment": "new", "search": "карбюратор"}}}'
        }
                             )

    def test_user_message(self):
        botpress_response_body = {
            "responses": [{"type": "text",
                           "text": '{"type":"user_message", "text":"test message"}'}]}

        elements_from_response = botpress.extract_elements(botpress_response_body)

        valid_cbapi_response = [
            {"type": "user_message",
             "text": "test message"}
        ]
        self.assertListEqual(valid_cbapi_response, elements_from_response)

    def test_expert_only_message(self):
        botpress_response_body = {
            "responses": [{"type": "text",
                           "text": '{"type":"expert_only", "text": "message for expert"}'}]}

        elements_from_response = botpress.extract_elements(botpress_response_body)

        valid_cbapi_response = [
            {"type": "expert_only",
             "text": "message for expert"}
        ]
        self.assertListEqual(valid_cbapi_response, elements_from_response)


class  AuthorizationTestSuite(unittest.TestCase):

    @unittest.skip
    def test_login_ok(self):
        auth_data = {
            "email": "i.averin@gmail.com",
            "password": "insect"
        }

        result = botpress.do_auth("http://localhost:3000", auth_data)
        self.assertEqual("success", result["status"])
        self.assertIn("token", result["payload"])
        self.assertEqual(True, botpress.is_auth)
        self.assertEqual(result["payload"]["token"], botpress.token)

    @unittest.skip
    def test_login_wrong_credentials(self):
        auth_data = {
            "email": "i.averin@gmail.com",
            "password": "insect11"
        }

        with self.assertRaises(Exception) as cm:
            result = botpress.do_auth("http://localhost:3000", auth_data)

        e = cm.exception
        self.assertEqual(type(e), requests.exceptions.HTTPError)

    @unittest.skip
    def test_login_connection_error(self):
        auth_data = {
            "email": "i.averin@gmail.com",
            "password": "insect11"
        }

        with self.assertRaises(Exception) as cm:
            result = botpress.do_auth("http://localhost:300", auth_data)

        e = cm.exception
        self.assertEqual(type(e), requests.exceptions.ConnectionError)

    @unittest.skip
    def test_post_authorized_request(self):
        auth_data = {
            "email": "i.averin@gmail.com",
            "password": "insect"
        }

        result = botpress.do_auth("http://localhost:3000", auth_data)
        token = result["payload"]["token"]

        result = botpress.post_authorized_request(
            "http://localhost:3000",
            "/api/v1/bots/flow-test1/converse/11212/secured?include=nlu,state,suggestions,decision",
            {"type": "text", "text": "hi"}, token)

        self.assertIn("responses", result)
        self.assertIn("state", result)
        self.assertIn("context", result["state"])

    @unittest.skip
    def test_wrong_auth_post_authorized_request(self):
        with self.assertRaises(requests.HTTPError) as cm:
            result = botpress.post_authorized_request(
                "http://localhost:3000",
                "/api/v1/bots/flow-test1/converse/11212/secured?include=nlu,state,suggestions,decision",
                {"type": "text", "text": "hi"}, "testtesttest")

        self.assertEqual(requests.HTTPError, type(cm.exception))
        self.assertEqual(401, cm.exception.response.json()["statusCode"])
        self.assertEqual('UnauthorizedError', cm.exception.response.json()["type"])

    def test_extract_node(self):
        self.assertEqual("choice-75da02", botpress._extract_flow_name("skills/choice-75da02.flow.json"))
        self.assertEqual("choice-75da02", botpress._extract_flow_name("choice-75da02"))

        self.assertEqual("choice-75da02.flow", botpress._extract_flow_name("skills/choice-75da02.flow.flow.json"))
        self.assertEqual("choice-75da02.flow", botpress._extract_flow_name("skills/choice-75da02.flow.flow.json"))
        self.assertEqual("skills/choice-75da02.flow", botpress._extract_flow_name("skills/choice-75da02.flow"))

        self.assertEqual("", botpress._extract_flow_name(""))

if __name__ == '__main__':
    unittest.main()
