# -*- coding: utf-8 -*-
import cbapi
from cbapi.response_builder import *
from cbapi.errors import *
import unittest


class ResponseBuilderTestSuite(unittest.TestCase):

    def test_single_text_response(self):
        resp = cbapi_text(text="Hello world!")
        self.assertDictEqual({"type": "text", "text": "Hello world!"}, resp)

        with self.assertRaises(CreateElementEmptyParameter) as cm:
            cbapi_text(text="")

        e = cm.exception
        self.assertEqual(type(e), CreateElementEmptyParameter)

    def test_single_typing_response(self):
        resp = cbapi_typing(value=True)
        self.assertDictEqual({"type": "typing", "value": True}, resp)

    def test_single_buttons_response(self):
        valid_buttons = {
            "type": "buttons",
            "title": "Choose next action",
            "buttons": [{
                "title": "Discover price",
                "payload": "PRICE"
            },
                {
                    "title": "Connect to operator",
                    "payload": "CONNECT"}
            ]}

        buttons_to_check = cbapi_buttons(title="Choose next action", buttons=valid_buttons["buttons"])
        self.assertDictEqual(valid_buttons, buttons_to_check)

        # empty text in button's title
        buttons_without_text = list(valid_buttons["buttons"])
        buttons_without_text[0]["title"] = ""

        with self.assertRaises(CreateElementEmptyParameter) as cm:
            cbapi_buttons(title="Test", buttons=buttons_without_text)
        e = cm.exception
        self.assertEqual(type(e), CreateElementEmptyParameter)

        # empty text in button's payload
        buttons_without_text = list(valid_buttons["buttons"])
        buttons_without_text[1]["payload"] = ""

        with self.assertRaises(CreateElementEmptyParameter) as cm:
            self.assertIsNone(cbapi_buttons(title="Test", buttons=buttons_without_text))
        e = cm.exception
        self.assertEqual(type(e), CreateElementEmptyParameter)

    def test_buttons_with_user_reply(self):
        valid_buttons = {
            "type": "buttons",
            "title": "Choose next action",
            "buttons": [{
                "title": "Title of button 1",
                "userReply":"reply 1",
                "payload": "REPLY1"
            },
                {
                    "title": "Title of button 2",
                    "userReply":"reply 2",
                    "payload": "REPLY2"},

                {"title": 'Usual title',
                 "payload": "REPLY3"},
            ]}

        buttons_to_check = cbapi_buttons(title="Choose next action", buttons=valid_buttons["buttons"])

        self.assertEqual(buttons_to_check["buttons"][0]["title"], "Title of button 1")
        self.assertEqual(buttons_to_check["buttons"][0]["userReply"], "reply 1")
        self.assertEqual(buttons_to_check["buttons"][0]["payload"], "REPLY1")

        self.assertEqual(buttons_to_check["buttons"][1]["title"], "Title of button 2")
        self.assertEqual(buttons_to_check["buttons"][1]["userReply"], "reply 2")
        self.assertEqual(buttons_to_check["buttons"][1]["payload"], "REPLY2")

        self.assertEqual(buttons_to_check["buttons"][2]["title"], "Usual title")
        self.assertEqual(buttons_to_check["buttons"][2]["payload"], "REPLY3")
        self.assertNotIn("userReply", buttons_to_check["buttons"][2])

    def test_response_from_elements(self):
        valid_cbapi_response = [{"type": "text", "text": "Hello World"}, {"type": "text", "text": "bye"},
                                {"type": "typing", "value": True},
                                {
                                    "type": "buttons",
                                    "title": "Choose your action",
                                    "buttons": [{
                                        "title": "Discover price",
                                        "payload": "PRICE"
                                    },
                                        {
                                            "title": "Connect to operator",
                                            "payload": "CONNECT"}
                                    ]}
                                ]

        full_response = response_from_elements("test_user", valid_cbapi_response)

        self.assertEqual("test_user", full_response["userId"])
        self.assertListEqual(valid_cbapi_response, full_response["responses"])

    def test_user_message(self):
        cbapi_response = {
            "type": "user_message",
            "text": "test message"
        }

        self.assertEqual(cbapi_response, cbapi_user_message("test message"))

    def test_expert_only_message(self):
        cbapi_response = {
            "type": "expert_only",
            "text": "test message"
        }

        self.assertEqual(cbapi_response, cbapi_expert_only_message("test message"))



if __name__ == '__main__':
    unittest.main()
