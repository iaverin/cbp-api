import unittest
import _config_default as config
import cbapi.core as core
import threading
import argparse

with_botpress_auth = True


class CoreTestCase(unittest.TestCase):

    def setUp(self) -> None:
        core.init(config, threading.Lock())

    @unittest.skip
    def test_do_auth(self):
        resp = core.do_auth(config.authorization)
        self.assertEqual("success", resp["status"])
        self.assertIn("token", resp["payload"])

        self.assertTrue(core.auth_data_equals(resp,
                                              {"payload":
                                                  {
                                                      "token": resp["payload"]["token"]
                                                  }
                                               })
                        )

    @unittest.skip
    def test_auth_request(self):
        core.global_auth_data = core.do_auth(config.authorization)

        lock = threading.Lock()
        resp = core.request_message("234234", {"type": "text", "text": "hi"}, core.global_auth_data)

        self.assertIn("state", resp)

if __name__ == '__main__':
    unittest.main()
