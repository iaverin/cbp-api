import unittest
from cbapi.profiler_helper import *


class ProfilerHelperTestCase(unittest.TestCase):

    def test_url_get_params_ok(self):
        url = "utm_source=yandex_adtrust&utm_medium=cpc&utm_campaign=ad&" \
              "utm_content=ad&utm_term=Бачок омывателя киа&src_type=search&" \
              "keyword=Бачок омывателя киа&compaign_id=35412443&" \
              "ad_id=5836099093&" \
              "test=%D0%BF%D1%80%D0%B8%D0%B2%D0%B5%D1%82"
        result = url_get_params(url)

        self.assertEqual("yandex_adtrust", result["utm_source"])
        self.assertEqual("Бачок омывателя киа", result["keyword"])
        self.assertEqual("привет", result["test"])

    def test_url_get_params_not_ok(self):
        url = "utm_source="
        result = url_get_params(url)

        self.assertEqual("", result["utm_source"])
        self.assertEqual({"sdf": ""}, url_get_params("sdf"))
        self.assertEqual({}, url_get_params(""))


if __name__ == '__main__':
    unittest.main()
