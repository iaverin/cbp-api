import cbapi as cb
from flask import Flask, request, jsonify, Response
import json
import requests
import threading
import cbapi
import profiler
import cbapi.botpress as botpress
from cbapi import core
from copy import copy

print("===========")

try:
    import config

except ModuleNotFoundError:
    print("config.py not found! ")
    print("Rename _cofing_default.py to config.py")
    exit(0)

BASE_SERVER = config.BASE_SERVER
BOT_ID = config.BOT_ID

authorisation = config.authorization if getattr(config, "authorization", False) else None
json.ensure_ascii = False

lock = threading.Lock()
app = Flask(__name__)
print("Using botpress at {}".format(BASE_SERVER))

core.init(config, lock)
if getattr(config, "authorization", False):
    print("Trying to authorize...")
    try:
        core.global_auth_data = core.do_auth(config.authorization)
        print("... OK")
    except Exception as e:
        print("... Could not authorize:" + str(e))
        print("Disable authorization or fix credentials")
        exit(0)



@app.route('/')
def hello_world():
    return 'Hello, Hell!'


@app.route('/echo', methods=['POST'])
def echo():
    # message = cb.mock_messages.get_random_mock_messages()
    message = request.json["message"]
    user_id = request.json["userId"]

    next_message = {
        "message": "<b>Echo:</b>" + message,
        "userId": user_id
    }
    return json.dumps(next_message, ensure_ascii=False)


def init_request(user_id, state, profile):

    profiler_response = profiler.profiler(state, profile)

    cbapi_response_elements = list()

    if profiler_response is None:
        return {}

    if "state" in profiler_response:
        cbapi_response_elements.append(cbapi.cbapi_system({"state": profiler_response["state"]}))

    request_payload = botpress.init_request(profiler_response, profile)

    resp = core.request_message(user_id, request_payload, core.global_auth_data)
    elements = botpress.extract_elements(resp)
    flow_state = botpress.extract_flow_state(resp)

    cbapi_response_elements.extend(elements)

    # return the parsed botpress message for user with injected system elements from profile
    response_to_client = cbapi.response_from_elements(user_id, cbapi_response_elements, flow_state)

    return response_to_client


@app.route('/requestBot', methods=['POST'])
def requestBot():
    print(request.json)

    req_type = request.json["type"]

    if req_type == "message":

        message = request.json["message"]
        user_id = request.json["userId"]

        payload = {
            "type": "text",
            "text": message
        }

        try:
            external_response = core.request_message(user_id, payload, core.global_auth_data)
            elements = botpress.extract_elements(external_response)
            flow_state = botpress.extract_flow_state(external_response)

            response_to_client = cbapi.response_from_elements(user_id, elements, flow_state)

        except Exception as e:
            print(e)
            response_to_client = {
                "message": "Error getting reply bot platform",
                "userId": user_id
            }

        response_data = json.dumps(response_to_client, ensure_ascii=False)

        resp = Response(response=response_data,
                        status=200,
                        mimetype="application/json")
        return resp

    if req_type == "init":
        user_id = request.json["userId"]
        state = request.json["state"]
        profile = request.json["profile"]

        response_data = json.dumps(init_request(user_id, state, profile), ensure_ascii=False)

        resp = Response(response=response_data,
                        status=200,
                        mimetype="application/json")

        return resp

    return None








