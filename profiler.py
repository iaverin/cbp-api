from cbapi.profiler_helper import *


def profiler(dialog_state: str, profile: dict):
    """:
    Returns the dict with the instructions for client or None. If returns None, so the botpress will be not requeted.

    dialog states:
        - new
        - bot
        - expert

    Args:
        dialog_state: string with the dialog state
        profile: dict with profile

    Returns: None or dict with the instructions to client as keys:
        - "state" - set's the state of dialog,
        - "flow" - initialize the flow on botpress
        - "node" - node within the flow

    """
    if profile is None or "visit" not in profile:
        profile["visit"] = "initial"

    if "flow" not in profile:
        profile["flow"] = None

    response = dict()
#
# here goes the profiling
#
    if dialog_state in ("expert", "bot"):
        if profile.get("search"):
            profile["urlGetParamsMain"] = ""
            response["state"] = "bot"
            response["flow"] = "2_2_searching_from_main"
            return response

    if dialog_state == "new":

        # flow 1: consulting request
        if profile.get("search") in ("", None) and \
                url_get_params(profile.get("urlGetParamsMain")).get("keyword") in ("", None):
            profile["urlGetParamsMain"] = ""
            response["state"] = "bot"
            response["flow"] = "1_1_zapros_kons"
            return response

        # flow 2 : I'm searching
        if profile.get("search") != "" and \
                url_get_params(profile.get("urlGetParamsMain")).get("keyword") == profile.get("search"):
            profile["urlGetParamsMain"] = ""
            response["state"] = "bot"
            response["flow"] = "2_1_searching"
            return response

        # profiling for testing
        if profile.get("visit") == "initial" and profile.get("flow") == "zapros_kons":
            response["state"] = "bot"
            response["flow"] = "1_1_zapros_kons"
            return response

        if profile.get("visit") == "initial" and profile.get("flow") == "virtual_expert":
            response["state"] = "bot"
            response["flow"] = "1_2_virtual_expert"
            return response

        if profile.get("visit") == "initial" and profile.get("flow") == "search":
            response["state"] = "bot"
            response["flow"] = "2_1_searching"
            return response

        # default case of wrong profile
        response["state"] = "expert"
        return response
    
    if dialog_state =="expert" and profile.get("not_taken") == "yes":
        response["state"] = "bot"
        response["flow"] = "missed_dialog"
        return response

    if profile["visit"] == "next":
        return None

    if profile["flow"] == "tutorial":
        response["flow"] = "tutorial"
        return response

    # please keep return None at the end of file
    return response


"""
A/B tests configuration
   
"""
"""
def abstest(user, d)->dict:
    pass

def profile_abstest(profile, dialog_state, user):
    response = dict()
    
    if profile is None or "visit" not in profile or profile["visit"] == "initial":
        
        if dialog_state == "new":
            response["state"] = "bot"
            response.update(abstest(user,{
                "test1":{
                    "id":"test1",
                    "weight":0.5,
                    "flow":"test1",
                    "profile":"test1",
                    "google_stat":{} 
                },
                "test2": {
                    "id": "test2",
                    "weight": 0.5,
                    "flow": "test2",
                    "profile": "test2",
                     "google_stat":{} 
            }}))

            return response
        return None

    if profile["visit"] == "next":
        return None
    
    if profile["profile"] == "test1" and profile["visit"] == ["next"] \
            and dialog_state == "bot_expired":
        response["state"] = "bot"
        response["flow"] = "bot_expired_1_contacts"
        return response

    if profile["profile"] == "test2" and profile["visit"] == ["next"] \
            and dialog_state == "bot_expired":
        response["state"] = "bot"
        response["flow"] = "bot_expired_2_contacts"
        return response

    return None
   
"""

if __name__== "__main__":
    print("Yo")
